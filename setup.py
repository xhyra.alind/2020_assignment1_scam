from setuptools import setup

setup(name='scam_me',
      version='0.2.1',
      description='SCAM Main Edition: Simple Crud Arrangement Manager',
      author='Mario Marino',
      author_email='m.marino35@campus.unimib.it',
      scripts=['scam/script/scam_me'],
      packages=['scam'],
      install_requires=['requests']
      )
