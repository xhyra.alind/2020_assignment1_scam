from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
import mysql.connector
import os


def db_connect():
    """

    @return:
    cur: database cursor,
    db: database object
    """
    db = mysql.connector.connect(
        host="assignment1db.mysql.database.azure.com",
        user="scam_backend@assignment1db",
        password=os.environ['DB_PWD'],
        database="scam"
    )

    cur = db.cursor()
    return cur, db


class Reservation(BaseModel):
    location: int
    name: str
    phone_number: str
    persons: int
    reservation: int  # reservation is a timestamp


app = FastAPI()  # intializing fastapi


@app.get("/destinations")
async def get_destinations():
    """

    @return:
    res: dict containing all possible destinations
    """
    cur, db = db_connect()
    cur.execute("SELECT * FROM locations")
    query_res = cur.fetchall()
    res = []
    for i in query_res:
        res.append({'id': int(i[0]), 'name': str(i[1]), 'capacity': int(i[2])})
    db.close()
    return res


@app.get("/reservation/{id}")
async def get_reservation(id: int):
    """

    @param id: integer representing reservation id
    @return:
    dict containing all information about the given reservation
    """
    cur, db = db_connect()
    cur.execute('SELECT * FROM arrangements WHERE id = {};'.format(id))
    query_res = cur.fetchall()
    db.close()
    if not query_res:
        raise HTTPException(status_code=404, detail="Not Found")
    query_res = query_res[0]
    return {'id': query_res[0],
            'location': query_res[1],
            'name': query_res[2],
            'phone_number': query_res[3],
            'persons': query_res[4],
            'reservation': query_res[5]}


@app.get("/reservations/{name}")
async def get_reservations(name: str):
    """

    @param name: string containg name of a person
    @return: dict containing all person's reservations
    """
    cur, db = db_connect()
    cur.execute('SELECT * FROM arrangements WHERE name = \'{}\';'.format(name.lower()))
    query_res = cur.fetchall()
    db.close()
    if not query_res:
        raise HTTPException(status_code=404, detail="Not Found")
    res = []
    for e in query_res:
        res.append({'id': e[0],
                    'location': e[1],
                    'name': e[2],
                    'phone_number': e[3],
                    'persons': e[4],
                    'reservation': e[5]})

    return res


@app.post("/reservation")
async def make_reservation(reservation: Reservation):
    """

    @param reservation: object containing all reservation info
    @return: reservation id
    """
    cur, db = db_connect()
    cur.execute(
        'INSERT ' +
        'INTO arrangements(location, name, phone_number, persons, reservation) VALUES ({}, \'{}\', {}, {}, {});'
        .format(reservation.location,
                reservation.name.lower(),
                reservation.phone_number,
                reservation.persons,
                reservation.reservation))

    db.commit()

    cur.execute('SELECT LAST_INSERT_ID();')

    res = cur.fetchall()
    db.close()

    return res[0][0]


@app.delete("/reservation/{id}")
async def delete_reservation(id: int):
    """

    @param id: reservation id
    @return: True on success, 404 error if id is non-existent
    """
    cur, db = db_connect()
    cur.execute('SELECT * FROM arrangements WHERE id = {};'.format(id))
    res = cur.fetchall()
    if not res:
        raise HTTPException(status_code=404, detail="Not Found")

    cur.execute('DELETE FROM arrangements WHERE id = {};'.format(id))
    db.commit()
    db.close()
    return True


@app.put("/reservation/{id}")
async def put_reservation(id: int, reservation: Reservation):
    """

    @param id: reservation id
    @param reservation: object containing all reservation info
    @return: True on success 404 error if id is non-existent
    """
    cur, db = db_connect()

    cur.execute('SELECT * FROM arrangements WHERE id = {};'.format(id))
    res = cur.fetchall()
    if not res:
        raise HTTPException(status_code=404, detail="Not Found")


    cur.execute(
        'UPDATE arrangements ' +
        'SET location = {}, name = \'{}\', phone_number = \'{}\', persons = {}, reservation = {} '
        .format(reservation.location,
                reservation.name.lower(),
                reservation.phone_number,
                reservation.persons,
                reservation.reservation) +
        'WHERE id = {};'.format(id)
    )
    db.commit()
    db.close()
    return True
