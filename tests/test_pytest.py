from scam.CommManager import CommManager
import mysql.connector
from scam.CLI import check_date, check_destination
import datetime as dt
import pytest
import unittest as ut
import os


def db_connect():
    db = mysql.connector.connect(
        host="assignment1db.mysql.database.azure.com",
        user="scam_backend@assignment1db",
        password=os.environ['DB_PWD'],
        database="scam"
    )

    cur = db.cursor()
    return cur, db


@pytest.mark.unit
def test_CLI_date():
    """
    Unit test used to test the check_date method of the CLI
    """

    # Assert a reservation date timestamp cannot be created for past events
    assert check_date(dt.datetime.now() - dt.timedelta(seconds=10)) is False, 'Error, output should be False'
    # Test a correct reservation date timestamp
    assert check_date(dt.datetime.now() + dt.timedelta(seconds=10)) is True, 'Error, output should be True'
    # Test a string instead of a timestamp
    assert check_date('ScamBus') is False, 'Error, output should be False'


@pytest.mark.unit
def test_CLI_destination():
    """
    Unit test used to test the check_destination method of the CLI
    """

    test_destinations = [{"id": 1}, {"id": 3}, {"id": 4}]
    # Test if the method returns the correct value when an element is in the collection
    assert check_destination(1, test_destinations) is True, "Error, output should be True"
    # Test if the method returns the correct value when an element in not in the collection
    assert check_destination(2, test_destinations) is False, "Error, output should be False"


@pytest.mark.integtest
def test_make_reservation():
    """
    Integration test used to test the integration within the commManager and the
    backend modules.
    This test covers the Create stage of the CRUD
    """

    # Create record to test
    cm = CommManager()
    res_id = cm.make_reservation(1, 'perosna', '314159265', 2, 1123581321)

    # Retrieve record from the DB
    cur, db = db_connect()
    cur.execute('SELECT * FROM arrangements WHERE id = \'{}\';'.format(str(res_id)))
    expected_result = cur.fetchall()
    db.close()
    output = expected_result[0]

    # Test record is created correctly using the retrieved record
    # The next four tests are used to test some of the parameters and verify
    # they are correct
    assert output[0] == res_id, 'Id should be {0} but instead is {1}'.format(str(res_id), str(output[0]))
    assert output[4] == 2, 'Persons should be 2 instead is {0}'.format(str(output[1]))
    assert output[1] == 1, 'Location should be 1 instead is {0}'.format(str(output[2]))
    assert output[2] == 'perosna', 'Name should be perosna instead is {0}'.format(output[3])

    # Delete record and test it cannot be deleted 2 times
    cm.delete_reservation(res_id)
    test_case_instance = ut.TestCase()
    test_case_instance.assertRaises(BaseException, cm.get_reservations, id=res_id)


@pytest.mark.integtest
def test_get_reservations():
    """
    Integration test used to test the integration within the commManager and the
    backend modules.
    This test covers the Read stage of the CRUD
    """

    # Create dummy record for the test
    cur, db = db_connect()
    cur.execute(
        'INSERT ' +
        'INTO arrangements(location, name, phone_number, persons, reservation) VALUES ({}, \'{}\', {}, {}, {});'
        .format(1,
                "testname",
                "123456789",
                1,
                953003655))
    db.commit()

    # Retrieve the dummy record id
    cur.execute('SELECT LAST_INSERT_ID();')
    res = cur.fetchall()
    db.close()
    created_record = res[0][0]

    # Retrieve and test dummy record using the record id to access it
    cm = CommManager()
    output = cm.get_reservations(created_record)
    # Test the attributes are correct for the dummy record
    assert output['id'] == created_record, 'Id should be {0} but instead is {1}'.format(str(created_record),                                                                                  str(output['id']))
    assert output['location'] == 1, 'Location should be 1 instead is {0}'.format(str(output['location']))
    assert output['persons'] == 1, 'Persons should be 1 instead is {0}'.format(str(output['persons']))
    assert output['name'] == "testname", 'Name should be instead is {0}'.format(output['name'])

    # Delete dummy record and test it cannot be deleted 2 times
    cm.delete_reservation(created_record)
    test_case_instance = ut.TestCase()
    test_case_instance.assertRaises(BaseException, cm.get_reservations, id=created_record)


@pytest.mark.integtest
def test_update_reservation():
    """
    Integration test used to test the integration within the commManager and the
    backend modules.
    This test covers the Update stage of the CRUD
    """

    # Create record to update and test
    cm = CommManager()
    res_id = cm.make_reservation(1, 'ambrosoidi', '111111111', 5, 1123581333)

    # Update the record
    cm.modify_reservation(res_id, 1, 'pappa', '24681012', 4, 1123581000)
    output = cm.get_reservations(res_id)

    # Test the record is updated
    assert output["id"] == res_id, 'Id should be {0} but instead is {1}'.format(str(res_id), str(output[0]))
    assert output["persons"] == 4, 'Persons should be 4 instead is {0}'.format(str(output[1]))
    assert output["location"] == 1, 'Location should be 1 instead is {0}'.format(str(output[2]))
    assert output["name"] == 'pappa', 'Name should be pappa instead is {0}'.format(output[3])

    # Delete record and test it cannot be deleted 2 times
    cm.delete_reservation(res_id)
    test_case_instance = ut.TestCase()
    test_case_instance.assertRaises(BaseException, cm.get_reservations, id=res_id)


@pytest.mark.integtest
def test_delete_reservation():
    """
    Integration test used to test the integration within the commManager and the
    backend modules.
    This test covers the Delete stage of the CRUD
    """

    # Create record to delete during test
    cm = CommManager()
    res_id = cm.make_reservation(1, 'thorderk', '987654321', 8, 1123581321)

    # Delete record and test delete works
    cm.delete_reservation(res_id)
    test_case_instance = ut.TestCase()
    test_case_instance.assertRaises(BaseException, cm.get_reservations, id=res_id)
