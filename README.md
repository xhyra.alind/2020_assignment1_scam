# 2020_assignment1_SCAM

SCAM_ME (Simple Crud Arrangement Manager Main Edition) is a simple CRUD software used to book reservations.
Repo: https://gitlab.com/xhyra.alind/2020_assignment1_scam

Members
-------

* Alind Xhyra 829865
* Mario Marino 829707
* Pietro Tropeano 829757


Project overview
----------------

SCAM implements the basic CRUD operations (create, read, update, delete) to book reservations. It's made of two Python
components (packages):
* frontend: a simple command line interface and a communication manager
* backend: offering an API for interacting with the MySQL DB

Install
-------
Run the following commands in your shell to install the package:\
`pip3 install scam_me`

then run `scam_me` to run the software.

Pipeline assumptions and considerations
---------------------------------------

We decided to work on a single branch (master branch) because we chose a simple and small project structure.

We chose to use a shared cache across all 7 pipeline stages to improve simplicity and minimize network and time usage.

The Continuous Delivery/Integration pipeline is made of seven stages:
* Build
* Verify
* Unit test
* Integration test
* Package
* Release
* Deploy

Each step will be addressed in the following sections.

### Build

Installs requirements.\
(Requirements will be cached and used in other stages of the pipeline)

### Verify

Runs static analysis of the code. 
To analyze the code we used Bandit and Prospector.

We skipped the Bandit test B322, which is a test used to prevent a vulnerability that allows code injections trough
input in python 2.
We decided to skip the test because the project is meant to be run on python 3 which is safe.

Prospector flags:   
* --no-autodetect (so it doesn't scan venv folders)
* --strictness low

### Unit test

Runs unit tests, testing two functions: **check_date()** and **check_destinations()**, which check respectively the
input date and destination code validity.

### Integration test

Runs integrations tests, testing the interaction between the CommManager and the backend (CRUD operations).
We implemented only a selection of meaningful tests for each stage of the CRUD.

### Package

Generates two wheel packages into "dist/" (artifact): scam_me (interactive interface) and scam_backend.

### Release

Publishes the scam_me wheel package on PyPI.

### Deploy

Connects via SSH to our Azure VM, copies scam_backend package from "dist/" and runs the server.
