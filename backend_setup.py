from setuptools import setup

setup(name='scam_backend',
      version='0.1',
      description='SCAM: Simple Crud Arrangement Manager',
      author='Mario Marino',
      author_email='m.marino35@campus.unimib.it',
      packages=['backend'],
      scripts=['backend/scam_backend'],
      install_requires=[
          'uvicorn',
          'fastapi',
          'pydantic',
          'mysql-connector-python'
      ]
      )
